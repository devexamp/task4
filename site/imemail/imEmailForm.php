<?php
if(substr(basename($_SERVER['PHP_SELF']), 0, 11) == "imEmailForm") {
	include '../res/x5engine.php';
	$form = new ImForm();
	$form->setField('Check In', @$_POST['imObjectForm_1_1'], '', false);
	$form->setField('Check Out', @$_POST['imObjectForm_1_2'], '', false);
	$form->setField('Adults', @$_POST['imObjectForm_1_3'], '', false);
	$form->setField('Kids', @$_POST['imObjectForm_1_4'], '', false);

	if(@$_POST['action'] != 'check_answer') {
		if(!isset($_POST['imJsCheck']) || $_POST['imJsCheck'] != 'C1F57E38E9A71CDB7697C7D796A58329' || (isset($_POST['imSpProt']) && $_POST['imSpProt'] != ""))
			die(imPrintJsError());
		$form->mailToOwner('example@example.com', 'example@example.com', '', '', false);
		@header('Location: ../index.html');
		exit();
	} else {
		echo $form->checkAnswer(@$_POST['id'], @$_POST['answer']) ? 1 : 0;
	}
}

// End of file